[ZASH](https://zash.business)

ARB to JSON converter

### Local
Run local by cloning project and running commands as follows
```bash
dart ./bin/arb.dart -i ./test.arb
dart ./bin/arb.dart -i ./test.arb -o ./output.json
```

### Installed / Built version
```bash
# Install the package globally
pub global activate --source git git@gitlab.com:15mbs/arb-converter.git

pub global run arb -i ./test.arb
```