import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:args/args.dart';

import 'package:colorize/colorize.dart';

const argInput = "input";
const argOutput = "output";
const argVerbose = "verbose";
const argHelp = "help";

ArgResults argResults;
bool isVerboseEnabled;

main(List<String> arguments) async {
  exitCode = 0; //Presume success

  final argsUsage = new ArgParser()
    ..addOption(argInput, abbr: 'i', help: 'Path to the .arb file to input')
    ..addOption(argOutput, abbr: 'o', help: 'Path where output should be placed. Can be a directory or json file')
    ..addFlag(argVerbose, abbr: 'v', help: 'Provide additional debug output')
    ..addFlag(argHelp, abbr: 'h', help: "Display this help information");

  argResults = argsUsage.parse(arguments);

  isVerboseEnabled = argResults[argVerbose];

  //Show help information
  if (argResults[argHelp]) {
      print("""

  ** HELP **
${argsUsage.usage}
      """);

      return;
  }

  log("Reading input file ${File(argResults[argInput]).absolute.path.replaceAll("./", "")}", LogLevel.info);
  String content = await getInput();
  log("Processing input...", LogLevel.info);
  Map<String, dynamic> output = await processInput(content);

  await writeOutput(output);
}

Future<String> getInput() async {
  String path = argResults[argInput];
  if (path == null) {
    error("You did not specify an arb file to convert. Use -h for help.");
    exit(1);
  }

  if (!(await FileSystemEntity.isFile(path))) {
    error("The specified path does not point to a file. Use -h for help.");
    exit(1);
  }

  String content = await new File(path)
      .readAsString();
  return content;
}

Map<String, dynamic> processInput(String content) {
  Map<String, dynamic> output = {};

  if (content.isNotEmpty) {
    Map<String, dynamic> input = json.decode(content);
    input.keys.forEach((String key) {
      if (key.indexOf('@') != 0) {
        output[key] = input[key];
      }
    });
  }

  return output;
}

Future writeOutput(Map<String, dynamic> content) async {
  try {
    File f = await outputFile;
    if (!(await f.exists())) {
      f.create(recursive: true);
    }

    f.writeAsString(json.encode(content));

    log("Wrote output to ${f.absolute.path.replaceAll("./", "")}", LogLevel.info);
  } catch (e) {
    error('Oops... Something went wrong: $e');
    exit(1);
  }
}

/**
 * Get / Create a reference to the output file
 */
Future<File> get outputFile async {
  String path = argResults[argOutput];
  if (path == null) {
    warn("You did not specify an output file. Directory of the input file is used by default.");
    path = fallbackOutput;
  }

  if (await FileSystemEntity.isDirectory(path)) {
    String lastChar = path.substring(path.length - 1, path.length);
    if (lastChar != '/') {
      path += '/';
    }

    path += outputFileName;
  }

  return new File(path);
}

/// Get the path to the output file.
/// If no output arg is set, place the output .json file in the same directory as the input file
String get outputFileName {
  String input = argResults[argInput];

  File f = new File(input);
  String parentPath = f.parent.path;
  String filename = input.replaceAll(parentPath + '/', '');
  filename = filename.replaceAll('.arb', '.json');
  return filename;
}

/// Path to the output file in the same directory as the input
String get fallbackOutput {
  String path = argResults[argInput];

  File f = new File(path);
  String parentPath = f.parent.path;
  return parentPath + '/' + outputFileName;
}


enum LogLevel {
  debug, info, warning, error
}

void error(String msg) {
  log(msg, LogLevel.error);
}

void warn(String msg) {
  log(msg, LogLevel.warning);
}

void log(String msg, LogLevel level) {
  if (isVerboseEnabled || level == LogLevel.error || level == LogLevel.warning) {
    print(_colorize(msg, level));
  }
}

String _colorize(String msg, LogLevel level) {
  Colorize c = Colorize(msg);

  switch (level) {
    case LogLevel.warning:
      c.yellow();
      break;
    case LogLevel.error:
      c.red();
      break;
    default:
      break;
  }

  return c.toString();
}